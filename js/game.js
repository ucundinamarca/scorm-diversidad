window.game={
    "slide1": {
    "situacion":`
            <h1>Situación 1</h1>
            <p>Antonio Presentó una solicitud a la universidad de Cundinamarca para conocer la oferta específica para la población sorda, siendo él parte de la comunidad usuaria de la lengua de señas colombiana (LSC), interesado en el programa de Ingeniería de Sistemas.</p>
            <p>Requiere acompañamiento permanente de interprete de lengua de señas, desde el proceso de admisión, desarrollo de actividades académicas, actividades culturales, y demás, pero la universidad no cuenta con estos servicios.</p>
    `, 
    "questions":[
        {
            "question": "1. Frente a los procesos fundamentales en la Educación, ¿con cuál se relaciona directamente la situación planteada?",
            "options": [
                "Permanencia",
                "Graduación",
                "Acceso",
                "Pertinencia",
                "Calidad"
            ],
            "correct":3,
            "checked":false
        },
    
        {
            "question": "2. ¿Cuáles barreras de contexto limitan el proceso académico del estudiante?",
            "options": [
                "Actitudinales",
                "Arquitectónicas",
                "Para el aprendizaje",
                "Comunicación y participación",
                "Tecnológicas"
            ],
            "correct":4,
            "checked":false
        },
        {
            "question": "3. ¿Cuáles son las acciones afirmativas que requiere implementar la universidad para garantizar la Educación?",
            "options": [
                "Definir un porcentaje mínimo de cupos orientado a estudiantes de poblaciones priorizadas, facilitando su acceso.",
                "Definir un programa de atención para minoría lingüística – Usuario de lengua de Señas Colombiana.",
                "Establecer acuerdos con los docentes en cuanto a la implementación de ajustes razonales en tiempos de ejecución y recursos."
            ],
            "correct":2,
            "checked":false
        }
    ]},
    "slide2": {
        "situacion":`
            <h1>Situación 2</h1>
            <p>Martín es un estudiante de 20 años del programa Ingeniería ambiental, en V semestre en la Universidad de Cundinamarca. Vive con sus padres y hermanos en el municipio de Madrid, Cundinamarca.</p>
            <p>Martín presenta una discapacidad visual de baja visión diagnosticada con reporte médico de atrofia macular óptica degenerativa como consecuencia de la Enfermedad de Stargardt, que consiste en la pérdida progresiva de la visión central asociada a una lesión macular central, esto genera una afectación en el campo visual y en la agudeza visual, ocasionando, en el caso de Martin, una pérdida del 90% del campo visual.</p>
            <p>Martín presenta dificultades para acceder a la información, teniendo en cuenta su capacidad visual: el campo visual reducido limita las lecturas y capacidad de atención a las explicaciones, requiere más tiempo para desarrollar las actividades académicas, presentan cansancio visual y agotamiento.</p>
        `, 
        "questions":[
            {
                "question": "1. Frente a los procesos fundamentales en la Educación, ¿con cuál se relaciona directamente la situación planteada?",
                "options": [
                    "Permanencia",
                    "Graduación",
                    "Acceso ",
                    "Pertinencia",
                    "Calidad "
                ],
                "correct":1,
                "checked":false
            },
            {
                "question": "2. ¿Cuáles barreras de contexto limitan el proceso académico del estudiante?",
                "options": [
                    "Actitudinales",
                    "Arquitectónicas",
                    "Para el aprendizaje ",
                    "Comunicación y participación",
                    "Tecnológicas"
                ],
                "correct":3,
                "checked":false
            },
            {
                "question": "3. ¿Cuáles son las acciones afirmativas que requiere implementar la universidad para garantizar la Educación?",
                "options": [
                    "Establecer protocolos de admisiones garantizando la disminución de barreras y el acceso la información.",
                    "Establecer acuerdos con los docentes en cuanto a la implementación de ajustes razonales en tiempos de ejecución, tipos de recursos académicos, y productos a entregar.",
                    "Definir un programa de acompañamiento personalizado para cada estudiante con discapacidad a través de las monitorias.",
                    "Disminuir el nivel de dificultad de las actividades asignadas."
                ],
                "correct":2,
                "checked":false
            }
    
        ],
    },
 
    "slide3":  {
        "situacion":`
            <h1>Situación 3</h1>
            <p>Manuel es un joven de 25 años perteneciente a una comunidad afrocolombiana, ubicada en el sector rural de Quibdó. Se presentó en la Universidad de Cundinamarca para el programa de Enfermería y actualmente vive en el municipio de Girardot, en una habitación en arriendo. Su proceso académico es bueno y no ha presentado dificultades para el desarrollo de las actividades, mostrando interés y compromiso.</p>
            <p>Manuel reconoce sus tradiciones, respeta y representa la cultura afro al interior del campus y sus compañeros, sin embargo, no cuenta con escenarios institucionales que le permitan fortalecer sus creencias y faciliten nuevas experiencias culturales.</p>
        `, 
        "questions":[
            {
                "question": "1. Frente a los procesos fundamentales en la Educación, ¿con cuál se relaciona directamente la situación planteada?",
                "options": [
                    "Permanencia",
                    "Graduación",
                    "Acceso",
                    "Pertinencia",
                    "Calidad"
                ],
                "correct":5,
                "checked":false
            },
            {
                "question": "2. ¿Cuáles barreras de contexto limitan el proceso académico del estudiante?",
                "options": [
                    "Actitudinales",
                    "Arquitectónicas",
                    "Para el aprendizaje",
                    "Comunicación y participación",
                    "Tecnológicas"
                ],
                "correct":1,
                "checked":false
            },
            {
                "question": "3. Cuáles son las acciones afirmativas que requiere implementar la universidad para garantizar la Educación?",
                "options": [
                    "Incentivar el intercambio cultural mediante escenarios de construcción dialógica y el reconocimiento de saberes ancestrales.",
                    "Establecer acuerdos con los docentes en cuanto a la implementación de ajustes razonables en tiempos de ejecución y recursos.",
                    "Definir un programa de acompañamiento personalizado para cada estudiante con discapacidad a través de las monitorias.",
                    "Ninguna, no es responsabilidad de la universidad promover la interculturalidad."
                ],
                "correct":1,
                "checked":false
            }
        ],
    },
    
    "slide4":  {
        "situacion":`
            <h1>Situación 4</h1>
            <p>Héctor Ramos Mavisoy de la comunidad Camëntša, ubicada en el municipio de Sibundoy, Putumayo, está interesado vincularse a la Universidad de Cundinamarca, al programa de Ingeniería ambiental.</p>
            <p>En conocimiento de las disposiciones de El Convenio Nº 169 de 1989 de la Organización Internacional del Trabajo, y las disposiciones dadas desde la constitución Política de 1991, Héctor considera que la universidad debe tener un acuerdo especial que garantice un % mínimo de población indígena matriculada, esperando poder obtener un cupo mediante esta modalidad, pero esto no se encuentra definido desde el protocolo de admisiones.</p>
        `, 
        "questions":[
            {
                "question": "1. Frente a los procesos fundamentales en la Educación, ¿con cuál se relaciona directamente la situación planteada?",
                "options": [
                    "Permanencia",
                    "Graduación",
                    "Acceso",
                    "Pertinencia",
                    "Calidad"
                ],
                "correct":3,
                "checked":false
            },
            {
                "question": "2. ¿Cuáles barreras de contexto limitan el proceso académico del estudiante?",
                "options": [
                    "Actitudinales",
                    "Arquitectónicas",
                    "Para el aprendizaje",
                    "Comunicación y participación",
                    "Tecnológicas"
                ],
                "correct":4,
                "checked":false
            },
            {
                "question": "3. ¿Cuáles son las acciones afirmativas que requiere implementar la universidad para garantizar la Educación?",
                "options": [
                    "Establecer protocolos de admisiones garantizando la disminución de barreras y el acceso la información.",
                    "Definir un porcentaje mínimo de cupos orientado a estudiantes de poblaciones priorizadas, facilitando su acceso.",
                    "Definir un programa de atención para minoría lingüística – Usuario de lengua de Señas Colombiana.",
                    "Establecer acuerdos con los docentes en cuanto a la implementación de ajustes razonales en tiempos de ejecución y recursos. "            
                ],
                "correct":2,
                "checked":false
            }
        ],
    },

    "slide5": 
        {
            "situacion":`
                <h1>Situación 5</h1>
                <p>La estudiante Camila Ramírez, del programa de Psicología es ciega.</p>
                <p>Dentro de las estrategias que ha definido la universidad se encuentra el acompañamiento permanente por parte de monitores, para ayudarle a desplazarse por el campus, realizar actividades y tareas académicas y acompañarla en sus sesiones de prácticas. Esta situación a limitado el desarrollo de la autonomía de la estudiante, la apropiación de su proceso y los procesos de socialización con compañeros y docentes. Camila puede requerir algunos apoyos, pero no se considera el acompañamiento permanente uno de ellos.</p>
            `, 
            "questions":[
                {
                    "question": "1. Frente a los procesos fundamentales en la Educación, ¿con cuál se relaciona directamente la situación planteada?",
                    "options": [
                        "Permanencia",
                        "Graduación",
                        "Acceso",
                        "Pertinencia",
                        "Calidad"
                    ],
                    "correct":2,
                    "checked":false
                },
                {
                    "question": "2. ¿Cuáles barreras de contexto limitan el proceso académico del estudiante?",
                    "options": [
                        "Actitudinales",
                        "Arquitectónicas",
                        "Para el aprendizaje",
                        "Comunicación y participación",
                        "Tecnológicas"
                    ],
                    "correct":1,
                    "checked":false
                },
                {
                    "question": "3. ¿Cuáles son las acciones afirmativas que requiere implementar la universidad para garantizar la Educación?",
                    "options": [
                        "Establecer protocolos de admisiones garantizando la disminución de barreras y el acceso la información.",
                        "Reconocer las capacidades de los estudiantes y promover el desarrollo de habilidades necesarias para responder a la vida laboral.",
                        "Definir un programa de acompañamiento personalizado para cada estudiante con discapacidad a través de las monitorias.",
                        "Disminuir el nivel de dificultad de las actividades asignadas."
                    ],
                    "correct":2,
                    "checked":false
                }
            ]
        }
}