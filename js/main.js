var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Educación inclusiva: diversidad y estrategias pedagógicas",
    autor: "Edilson Laverde Molina",
    date: "12/07/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [{
    url: "sonidos/click.mp3",
    name: "clic"
}];
var info_ova = {
    objetivo: {
        title: "Objetivo",
        description: "Implementar los conocimientos básicos construidos."
    },
    instruccion: {
        title: "Instrucción",
        description: "Dé clic en cada situación, léala y escúchela con atención . Finalmente, responda a las preguntas. Ánimo y adelante."
    },
    creditos: `
        <div style="margin-top:-100px;" class="creditos">
            <h2>Diplomado</h2>
                <h1>DIVERSIDAD COMO PROPÓSITO EDUCATIVO</h3>
            <h2>Nombre del material</h2>
                <h1>IMPLEMENTEMOS</h1>
            <h2>Experto en contenido</h2>
                <h1>Yehimy Jazmin Caballero Rodríguez</h1>
                <h1 style="margin-top:-6px">Cindy Paola Quevedo Rocha</h1>
            <h2>Diseño y desarrollo</h2>
            <h2>Área de producción de recursos educativos digitales.</h2>
            <h2>Asesor pedagógico</h2>
                <h1>Lida Consuelo Rincón Méndez</h1>
            <h2>Diseño gráfico</h2>
                <h1>Nazly María Victoria Díaz Vera</h1>
            <h2>Programador</h2>
                <h1>Edilson Laverde Molina</h1>
            <h2>Oficina de Educación Virtual y a Distancia</h2>
            <h2>Universidad de Cundinamarca</h2>
            <h2>2021</h2>
        </div>
    `
};
var index_game=0;
var index_game_grupo=0;
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(".btn_f").css("cursor", "pointer");
                ivo(ST + "preload").hide();
                t.events();
                t.animation();
                stage1.play();

            });
        },
        methods: {
            Analyser_Finished:function(){
                let finish = true;
                let point=0;
                for(let i=1; i<=5; i++){
                    let questions = game["slide"+i].questions;
                    for(q of questions){
                        if(!q.checked){
                            finish = false;
                            break;
                        }
                        if(q.correct==q.user){
                            point=point + 1;
                        }
                    }
                }
                if(finish){
                    let puntaje =  ( 50/15 ) * point;
                    Scorm_mx = new MX_SCORM(false);
                    console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id+ " Nota: "+puntaje);
                    Scorm_mx.set_score(puntaje);
                  
                }
            },
            events: function () {
                var t = this;
                ivo(".btn1, .btn_f").on("click", function () {
                    let id = ivo(this).attr("id");
                    stage1.timeScale(4).reverse();
                    ivo.play("clic");
                    if(id.indexOf("act")>0){
                        stage2.timeScale(1).play();
                    }
                    if(id.indexOf("ins")>0){
                        stage4.timeScale(1).play();
                        ivo(ST+"textos").html(`<div class="info">
                                                <h1>${info_ova.instruccion.title}</h1>
                                                <p>${info_ova.instruccion.description}</p>
                                              </div>`);
                    }
                    if(id.indexOf("obj")>0){
                        stage4.timeScale(1).play();
                        ivo(ST+"textos").html(`<div class="info">
                                                <h1>${info_ova.objetivo.title}</h1>
                                                <p>${info_ova.objetivo.description}</p>
                                              </div>`);
                    }
                    if(id.indexOf("cred")>0){
                        stage4.timeScale(1).play();
                        ivo(ST+"textos").html(`<p>${info_ova.creditos}</p>`);
                    }
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(ST+"home-1").on("click", function () {
                    stage2.reverse().timeScale(4);
                    setTimeout(()=>{
                        console.log("ejecutandome");
                        stage1.play().timeScale(1);
                    },1000)
                    
                    
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                
                ivo(".btn2").on("click", function () {
                    let id = ivo(this).attr("id");
                    ivo(this).css("opacity","0.5");
                    ivo(".icon").hide();
                    switch(id){
                        case  S+"btn_sit_1":
                            index_game=1;
                            ivo(ST+"icon-pregunta-1").show();
                        break;
                        case  S+"btn_sit_2":
                            index_game=2;
                            ivo(ST+"icon-pregunta-2").show();
                        break;
                        case  S+"btn_sit_3":
                            index_game=3;
                            ivo(ST+"icon-pregunta-3").show();
                        break;
                        case  S+"btn_sit_4":
                            index_game=4;
                            ivo(ST+"icon-pregunta-4").show();
                        break;
                        case  S+"btn_sit_5":
                            index_game=5;
                            ivo(ST+"icon-pregunta-5").show();
                        break;
                    }
                    //carga la situacion//
                    ivo(ST+"situacion-texto").html(`<p>${game["slide"+index_game].situacion}</p>`);
                    ivo.play("clic");
                    stage3.timeScale(1).play();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(".btn3").on("click", function () {
                        let id = ivo(this).attr("id");
                        ivo.play("clic");
                        switch (id) {
                            case S + "btn-1":
                                index_game_grupo = 1;
                                break;
                            case S + "btn-2":
                                index_game_grupo = 2;
                                break;
                            case S + "btn-3":
                                index_game_grupo = 3;
                                break;

                        }
                        if (game["slide" + index_game].questions[index_game_grupo - 1].checked == false) {
                            let html = `<p>${game["slide"+index_game].questions[index_game_grupo-1].question}</p>
                                        <ul class="options">`;
                            //carga las preguntas//
                            let n = 1;
                            for (let pregunta of game["slide" + index_game].questions[index_game_grupo - 1].options) {
                                html += `<li data-indexGame="${index_game}" data-indexGameGrupo="${index_game_grupo-1}" data-n="${n}" data-selected="false">${pregunta}</li>`;
                                n += 1;
                            }
                            html += "</ul>";
                            

                            ivo(ST + "pregunta-texto").html(html);
                            ivo(".options li").on("click", function () {
                                let index_game1 = parseInt(ivo(this).attr("data-indexGame"));
                                let index_game_grupo1 = parseInt(ivo(this).attr("data-indexGameGrupo"));
                                game["slide" + index_game1].questions[index_game_grupo1].user = parseInt(ivo(this).attr("data-n"));
                                game["slide" + index_game1].questions[index_game_grupo1].checked = true;
                                ivo(".options li").removeClass("active");
                                ivo(".options li").attr("data-selected", "false");
                                let sel = ivo(this).attr("data-selected");
                                if (sel == "false") {
                                    ivo(this).attr("data-selected", "true");
                                    ivo(this).addClass("active");
                                } else {
                                    ivo(this).attr("data-selected", "false");
                                    ivo(this).removeClass("active");
                                }
                                ivo(ST + "pregunta-texto").html("<p class='resuleto'>Pregunta contestada</p>");
                                t.Analyser_Finished();
                            });
                        }else{
                            ivo(ST + "pregunta-texto").html("<p class='resuleto'>Pregunta contestada</p>");
                        }

                        question.timeScale(1).play();

                    })
                    .on("mouseover", function () {

                    })
                    .on("mouseout", function () {

                });
                ivo(ST+"close").on("click", function () {
                    ivo.play("clic");
                    question.timeScale(1).reverse();
                });
                ivo(ST+"btn-atras").on("click", function () {
                    ivo.play("clic");
                    stage3.timeScale(4).reverse();
                    question.timeScale(4).reverse();
                });
                ivo(ST+"btn-atras2").on("click", function () {
                    ivo.play("clic");
                    stage4.timeScale(4).reverse();
                    stage1.timeScale(1).play();
                });
                
            },
            animation: function () {
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST + "stage-1",     .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "container-1", .8, {y: -300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(".btn1", .5,     {y: 50,  opacity: 0}, .2), 0);
                stage1.append(TweenMax.from(ST + "avatar1", .8,     {x: -200, opacity: 0}), 0);
                stage1.stop();
            
                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST + "stage-2", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "container-2", .8, {x: 400, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "avatar2", .8,     {x: -200, opacity: 0}), 0);
                stage2.append(TweenMax.staggerFrom(ST+"label-1 div", .2,     {x: 150,  opacity: 0}, .1), 0);
                stage2.append(TweenMax.staggerFrom(".btn2", .4,     {y: 150, rotation:900, x:-200, opacity: 0}, .05), 0);
                stage2.append(TweenMax.from(ST + "home-1", .8,     {x: -200, opacity: 0}), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.from(ST + "stage-3", .8, {x: 1300, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "container-3", .8, {x: 400, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "avatars", .8,     {x: -200, opacity: 0}), 0);
                stage3.append(TweenMax.staggerFrom(ST+"label-2 div", .2,     {x: 150,  opacity: 0}, .1), 0);
                stage3.append(TweenMax.from(ST + "container-situaciones", .8,     {x: -200, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "container-preguntas", .8,     {x: -200, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "btn-atras", .8,     {x: -200, opacity: 0}), 0);
                stage3.stop();

                question = new TimelineMax();
                question.append(TweenMax.from(ST + "container-pregunta", .8, {scale: 0, opacity: 0, rotation:800}), 0);
                question.append(TweenMax.from(ST + "close", .8, {scale: 0, opacity: 0, rotation:900}), 0);
                question.stop();

                stage4 = new TimelineMax();
                stage4.append(TweenMax.from(ST + "stage-4", .8, {x: 1300, opacity: 0}), 0);
                stage4.append(TweenMax.from(ST + "container-4", .8, {x: 400, opacity: 0}), 0);
                stage4.append(TweenMax.staggerFrom(ST+"label-3 div", .2,     {x: 150,  opacity: 0}, .1), 0);
                stage4.append(TweenMax.from(ST + "avatar3", .8,     {x: -200, opacity: 0}), 0);
                stage4.append(TweenMax.from(ST + "container-textos", .8,     {y: 200, opacity: 0}), 0);
                stage4.append(TweenMax.from(ST + "btn-atras2", .8,     {x: -200, opacity: 0}), 0);
                stage4.stop();

                stage5 = new TimelineMax();
                stage5.append(TweenMax.from(ST + "stage-5", .8, {x: 1300, opacity: 0}), 0);
                stage5.stop();
            }
        }
    });
}