(function() {
    /* eslint no-undef:0, semi:2 */
    /* eslint guard-for-in:0, semi:2*/
    if (typeof ivo === "undefined") {
      window.ivo = {};
    }
    var eventQueue = [];
    document.addEventListener('DOMContentLoaded', function () {
      eventQueue.forEach(function (fn) {
        fn();
      });
    }, false);
    ivo = function (arg) {
      var htmlEls;
      if (arg instanceof Function) {
        eventQueue.push(arg);
        return document;
      } else if(arg instanceof Object) {
        return new vue([arg]);
      } else {
        if (arg instanceof HTMLElement) {
          htmlEls = [arg]
        } else {
          matches = arg.match(/^<(\w+)>ivo/);
          if (matches) {
            htmlEls = [document.createElement(matches[1])];
          } else {
            htmlEls = Array.prototype.slice.call(document.querySelectorAll(arg));
          }
        }
        return new vue(htmlEls);
      }
    };
    vue = function (elements) {
      this.elements = elements;
      return this;
    };
    vue.prototype.html = function (string) {
      if (typeof string !== 'undefined') {
        this.elements.forEach(function (el) {
          el.innerHTML = string;
        });
        return this;
      } else {
        return this.elements[0].innerHTML;
      }
    };
    vue.prototype.empty = function () {
      this.html("");
      return this;
    };
    vue.prototype.appendToFirst = function (arg) {
      this.elements[0].appendChild(arg.cloneNode(true));
    };
    vue.prototype.append = function (arg) {
      if (arg instanceof vue) {
        arg.elements.forEach(function (el) {
          this.elements[0].appendChild(el.cloneNode(true));
        }.bind(this));
      } else if (arg instanceof HTMLElement) {
        this.elements[0].appendChild(arg.cloneNode(true));
      } else if (typeof arg === "string") {
        this.elements.forEach(function (el) {
          el.innerHTML += arg;
        });
      }
      return this;
    };
    vue.prototype.attr = function (name, value) {
      if (typeof value !== 'undefined') {
        this.elements.forEach( function(el) {
          el.setAttribute(name, value);
        });
        return this;
      } else {
        return this.elements[0].getAttribute(name);
      }
    };
    vue.prototype.get = function(index) {
      return new vue([this.elements[index]]);
    };
    vue.prototype.addClass = function (newClass) {
      this.elements.forEach( function (el) {
        el.classList.add(newClass);
      });
      return this;
    };
    vue.prototype.removeClass = function (dropClass) {
      this.elements.forEach( function (el) {
        el.classList.remove(dropClass);
      });
      return this;
    };
    vue.prototype.children = function () {
      var allChildren = [], newChildren;
      this.elements.forEach(function (el) {
        newChildren = Array.prototype.slice.call(el.children);
        allChildren = allChildren.concat(newChildren);
      });
      return new vue(allChildren);
    };
    vue.prototype.parent = function () {
      var parents = [], currentParent;
      this.elements.forEach(function (el) {
        // debugger
        currentParent = el.parentElement;
        if (parents.indexOf(currentParent) === -1) {
          parents.push(currentParent);
        }
      });
      return new vue(parents);
    };
    vue.prototype.find = function (selector) {
      var matchingElements = [], currentMatchesQuery, currentMatches;
      this.elements.forEach(function (el) {
        currentMatchesQuery = el.querySelectorAll(selector);
        currentMatches = Array.prototype.slice.call(currentMatchesQuery);
        currentMatches.forEach( function (match) {
          if (matchingElements.indexOf(match) === -1) {
            matchingElements.push(match);
          }
        });
      });
      return new vue(matchingElements);
    };
    vue.prototype.remove = function () {
      this.elements.forEach(function (el) {
        el.remove();
      });
    };
    vue.prototype.on = function (type, callback) {
      this.elements.forEach(function (el) {
        el.addEventListener(type, callback);
      });
      return this;
    };
    vue.prototype.off = function (type, callback) {
      this.elements.forEach(function (el) {
        el.removeEventListener(type, callback);
      });
      return this;
    };
    vue.prototype.each = function(callback) {
      this.elements.forEach(callback);
    };
    vue.prototype.hide = function() {
      this.originalDisplay = this.css("display");
      this.css("display", "none");
      return this;
    };
    vue.prototype.show = function() {
      newDisplay = this.originalDisplay || "block";
      this.css("display", newDisplay);
      return this;
    };
    vue.prototype.css = function(property, value) {
      if (typeof value === 'undefined') {
        return this.elements[0].style.getPropertyValue(property);
      } else {
        this.elements.forEach(function(element){
          element.style[property] = value;
        });
        return this;
      }
    };
    vue.prototype.text = function(string) {
      if (typeof string !== 'undefined') {
        this.elements.forEach(function (el) {
          el.innerText = string;
        });
        return this;
      } else {
        text = "";
        this.elements.forEach(function(element){
          text += element.innerText;
        });
        return text;
      }
    };
    vue.prototype.animateClass=function(options, callback) {
        this.elements.forEach(function(element){
            const node =element;
            for(var i=0; i<options.effect.length; i++){
                node.classList.add(options.effect[i]);
            }
            var toEnd=function() {
                if(options.autoremove){
                    for(var i=0; i<options.effect.length; i++){
                        node.classList.remove(options.effect[i]);
                    }
                    node.removeEventListener('animationend', toEnd);
                }
                if (typeof callback === 'function') callback();
            }
            node.addEventListener('animationend', toEnd)
        });
    }
    vue.prototype.slider= function (options) {
        var defaults = {
            list:[],
            progress:"ivoslider",
            slides: "",
            width:1200
        };
        //extendemos propiedades//
        ivo.extend(defaults, options);
        //extaemos los slides que hay//
        defaults.list[defaults.id]=[];
        var slides = document.querySelectorAll(defaults.slides);
        slides.forEach(function(item,index) {
            //los reposicionamos hacia la izquierda//
            item.style.left=defaults.width*index+"px";
            defaults.list[defaults.id].push((defaults.width*index)*-1);
        });
        //establecemos primer slide//
        window[defaults.progress]=1;
        this.go=function(go){
          this.rules("+",go);
        }
        this.rules=function(action,go=null){
            if(go!=null){
              window[defaults.progress]=parseInt(go);
            }else{
              //establecemos si quiere avanzar o retorceder//
              if(action=="+"){window[defaults.progress]+=1}
              if(action=="-"){window[defaults.progress]-=1}
            }
            //establecemos la ubicacion a llegar con la libreria greensock//
            var index=window[defaults.progress]-1;
            TweenMax.to("#"+slides[0].parentNode.id, .8, {x:defaults.list[defaults.id][index]}, .5);
            //un callback que devuelbe el numepro de lagina del slider//
            if(action=="+" || action=="-"){defaults.onMove( window[defaults.progress]);}
            //analisis del boton atras//
            if(window[defaults.progress]==1) {
               ivo(defaults.btn_back).hide() }else{ ivo(defaults.btn_back).show();}
            //analisis del boton siguinete con callabak onfinish//
            if(window[defaults.progress]==slides.length) {
                ivo(defaults.btn_next).hide(); 
                defaults.onFinish();   
            } else{ 
                ivo(defaults.btn_next).show();       
            } 
        }
        var t=this;
        //activamos las reglas sin parametros para que de entrada se quite el boton de atras//
        t.rules();
        //eventos botones//
        ivo(defaults.btn_next).on("click",function(){
            t.rules("+");
        }).css("cursor","pointer");
        ivo(defaults.btn_back).on("click",function(){
            t.rules("-");
        }).css("cursor","pointer");
        return this;
    },
    ivo.extend = function (base) {
      var objects = Array.prototype.slice.call(arguments, 1);
      objects.forEach( function (object) {
        for (var attribute in object) {
          base[attribute] = object[attribute];
        }
      });
    };
    var loadXMLDoc = function(options) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == XMLHttpRequest.DONE){
          if (xmlhttp.status == 200) {
            options.success(xmlhttp.response);
          } else {
            options.error();
          }
        }
      };
      xmlhttp.open(options.method, options.url, true);
      if (options.method == 'POST') {
        xmlhttp.setRequestHeader('Content-type', options.contentType);
      }
      xmlhttp.send(options.data);
    };
    ivo.ajax = function (options) {
      var defaults = {
        success: function(){},
        error: function(){},
        url: window.location.href,
        method: "GET",
        data: "",
        contentType: "application/x-www-form-urlencoded"
      };
      ivo.extend(defaults, options);
      loadXMLDoc(defaults);
    };
    ivo.get = function(url, successCallback) {
      ivo.ajax({
        url: url,
        success: successCallback
      });
    };
    ivo.post = function(url, data, successCallback) {
      ivo.ajax({
        url: url,
        data: data,
        success: successCallback
      });
    };
    ivo.info=function(options){
        var defaults = {
            title: "",
            icon: ""
        };
        ivo.extend(defaults, options);
        var direction = null;
        var titleText = defaults.title + " ";
        var self = this;
        //icono pagina//
        var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = defaults.icon;
        document.getElementsByTagName('head')[0].appendChild(link);
        //titulo rotativo//
        document.title = titleText;
        var style = 'color: red; font-size: 13px; background: rgb(255, 255, 219); padding: 1px 5px; border: 1px solid rgba(0, 0, 0, 0.1);';
        console.info('%cDeveloper: ' + options.autor, style);
        console.info('%cFecha: ' + options.date, style);
        console.info('%cEmail: ' + options.email, style);
        console.info('%cFacebook: ' + options.facebook, style);
    }
    ivo.structure=function(structure){
        var init={created:structure.created};
        var render= Object.assign(structure.methods, init);
        render.created();
    }
    var ivoPlaylist=null;
    ivo.load_audio=function(audios,callback,index){
        index = typeof index !== 'undefined' ?  index : 0;
        if(audios.length==index){
            ivoPlaylist=audios;
            callback();
        }else{
            window[audios[index].name] = new Howl({
                src: [audios[index].url],
                onload: function onload() {
                    index += 1;
                    ivo.load_audio(audios,callback,index);
                }
            });
        }
    }
    ivo.play=function(audio){
        for (var i = 0; i <  ivoPlaylist.length; i++) {
            window[ivoPlaylist[i].name].stop();
        }
        
        window[audio].play();
    }
  }());
