/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage-1',
                            type: 'group',
                            rect: ['-6', '-15', '1030', '674', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-1',
                                type: 'image',
                                rect: ['6px', '15px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-1.png",'0px','0px']
                            },
                            {
                                id: 'avatar1',
                                symbolName: 'avatar',
                                type: 'rect',
                                rect: ['0', '290', '424', '384', 'auto', 'auto']
                            },
                            {
                                id: 'container-1',
                                type: 'group',
                                rect: ['388px', '0px', '469', '486', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'back-container',
                                    type: 'image',
                                    rect: ['0px', '0px', '469px', '484px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_4.png",'0px','0px']
                                },
                                {
                                    id: 'btn_obj-1',
                                    type: 'image',
                                    rect: ['25px', '342px', '148px', '144px', 'auto', 'auto'],
                                    clip: 'rect(0px 117px 117px 26px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px'],
                                    userClass: "btn1"
                                },
                                {
                                    id: 'btn_ins-1',
                                    type: 'image',
                                    rect: ['159px', '342px', '148px', '144px', 'auto', 'auto'],
                                    clip: 'rect(0px 117px 117px 26px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px'],
                                    userClass: "btn1"
                                },
                                {
                                    id: 'btn_act-1',
                                    type: 'image',
                                    rect: ['293px', '342px', '148px', '144px', 'auto', 'auto'],
                                    clip: 'rect(0px 117px 117px 26px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px'],
                                    userClass: "btn1"
                                }]
                            }]
                        },
                        {
                            id: 'stage-2',
                            type: 'group',
                            rect: ['-6', '0', '1040', '659', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-2',
                                type: 'image',
                                rect: ['6px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-2.png",'0px','0px']
                            },
                            {
                                id: 'btn_sit_1',
                                type: 'image',
                                rect: ['333px', '182px', '200px', '194px', 'auto', 'auto'],
                                clip: 'rect(0px 163px 156px 37px)',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_5.png",'0px','0px'],
                                userClass: "btn2"
                            },
                            {
                                id: 'btn_sit_2',
                                type: 'image',
                                rect: ['496px', '182px', '200px', '194px', 'auto', 'auto'],
                                clip: 'rect(0px 163px 156px 37px)',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_6.png",'0px','0px'],
                                userClass: "btn2"
                            },
                            {
                                id: 'btn_sit_3',
                                type: 'image',
                                rect: ['650px', '183px', '200px', '194px', 'auto', 'auto'],
                                clip: 'rect(0px 163px 156px 37px)',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px'],
                                userClass: "btn2"
                            },
                            {
                                id: 'btn_sit_4',
                                type: 'image',
                                rect: ['432px', '377px', '200px', '194px', 'auto', 'auto'],
                                clip: 'rect(0px 163px 156px 37px)',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_8.png",'0px','0px'],
                                userClass: "btn2"
                            },
                            {
                                id: 'btn_sit_5',
                                type: 'image',
                                rect: ['568px', '377px', '200px', '194px', 'auto', 'auto'],
                                clip: 'rect(0px 163px 156px 37px)',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_9.png",'0px','0px'],
                                userClass: "btn2"
                            },
                            {
                                id: 'label-1',
                                type: 'group',
                                rect: ['102px', '0', '928', '129', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'sub_label_1',
                                    type: 'image',
                                    rect: ['510px', '61px', '418px', '68px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px']
                                },
                                {
                                    id: 'label_1',
                                    type: 'image',
                                    rect: ['0px', '0px', '928px', '61px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'avatar2',
                                symbolName: 'avatar',
                                type: 'rect',
                                rect: ['0px', '275px', '424', '384', 'auto', 'auto']
                            },
                            {
                                id: 'home-1',
                                type: 'image',
                                rect: ['18px', '14px', '77px', '95px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_11.png",'0px','0px']
                            },
                            {
                                id: 'container-2',
                                type: 'group',
                                rect: ['853px', '240', '187', '410', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'opacity-1',
                                    type: 'image',
                                    rect: ['0px', '0px', '187px', '410px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_12.png",'0px','0px']
                                },
                                {
                                    id: 'btn_cred-1',
                                    type: 'image',
                                    rect: ['64px', '286px', '58px', '73px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_10.png",'0px','0px'],
                                    userClass: "btn_f"
                                },
                                {
                                    id: 'btn_obj-2',
                                    type: 'image',
                                    rect: ['19px', '15px', '148px', '144px', 'auto', 'auto'],
                                    clip: 'rect(0px 119px 117px 27px)',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px'],
                                    userClass: "btn_f"
                                },
                                {
                                    id: 'btn_ins-2',
                                    type: 'image',
                                    rect: ['19px', '147px', '148px', '144px', 'auto', 'auto'],
                                    clip: 'rect(0px 119px 117px 27px)',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px'],
                                    userClass: "btn_f"
                                }]
                            }]
                        },
                        {
                            id: 'stage-3',
                            type: 'group',
                            rect: ['0', '0', '1034', '650', 'auto', 'auto'],
                            c: [
                            {
                                id: 'base',
                                type: 'group',
                                rect: ['0', '0', '1034', '650', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo-3',
                                    type: 'image',
                                    rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"fondo-2.png",'0px','0px']
                                },
                                {
                                    id: 'label-2',
                                    type: 'group',
                                    rect: ['96px', '0', '928', '129', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'sub_label_2',
                                        type: 'image',
                                        rect: ['510px', '61px', '418px', '68px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px']
                                    },
                                    {
                                        id: 'label_2',
                                        type: 'image',
                                        rect: ['0px', '0px', '928px', '61px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'container-3',
                                    type: 'group',
                                    rect: ['847px', '240', '187', '410', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'opacity-3',
                                        type: 'image',
                                        rect: ['0px', '0px', '187px', '410px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_12.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn_cred-3',
                                        type: 'image',
                                        rect: ['64px', '286px', '58px', '73px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_10.png",'0px','0px'],
                                        userClass: "btn_f"
                                    },
                                    {
                                        id: 'btn_obj-3',
                                        type: 'image',
                                        rect: ['19px', '15px', '148px', '144px', 'auto', 'auto'],
                                        clip: 'rect(0px 119px 117px 27px)',
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px'],
                                        userClass: "btn_f"
                                    },
                                    {
                                        id: 'btn_ins-3',
                                        type: 'image',
                                        rect: ['19px', '147px', '148px', '144px', 'auto', 'auto'],
                                        clip: 'rect(0px 119px 117px 27px)',
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px'],
                                        userClass: "btn_f"
                                    }]
                                }]
                            },
                            {
                                id: 'avatars',
                                type: 'group',
                                rect: ['5', '283', '357', '666', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'icon-pregunta-5',
                                    type: 'image',
                                    rect: ['0px', '0px', '357px', '666px', 'auto', 'auto'],
                                    clip: 'rect(0px 357px 359px 0px)',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_28.png",'0px','0px'],
                                    userClass: "icon"
                                },
                                {
                                    id: 'icon-pregunta-4',
                                    type: 'image',
                                    rect: ['0px', '0px', '355px', '588px', 'auto', 'auto'],
                                    clip: 'rect(0px 357px 359px 0px)',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_27.png",'0px','0px'],
                                    userClass: "icon"
                                },
                                {
                                    id: 'icon-pregunta-3',
                                    type: 'image',
                                    rect: ['0px', '0px', '355px', '400px', 'auto', 'auto'],
                                    clip: 'rect(0px 355px 400px 0px)',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_26.png",'0px','0px'],
                                    userClass: "icon"
                                },
                                {
                                    id: 'icon-pregunta-2',
                                    type: 'image',
                                    rect: ['0px', '0px', '355px', '368px', 'auto', 'auto'],
                                    clip: 'rect(0px 355px 368px 0px)',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px'],
                                    userClass: "icon"
                                },
                                {
                                    id: 'icon-pregunta-1',
                                    type: 'image',
                                    rect: ['0px', '0px', '355px', '390px', 'auto', 'auto'],
                                    clip: 'rect(0px 355px 390px 0px)',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px'],
                                    userClass: "icon"
                                }]
                            },
                            {
                                id: 'container-situaciones',
                                type: 'group',
                                rect: ['359', '134', '463', '358', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo-situaciones',
                                    type: 'image',
                                    rect: ['0px', '0px', '463px', '358px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                                },
                                {
                                    id: 'situacion-texto',
                                    type: 'rect',
                                    rect: ['23px', '22px', '416px', '314px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                },
                                {
                                    id: 'container-pregunta',
                                    type: 'group',
                                    rect: ['23', '-5', '457', '341', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'pregunta-texto',
                                        type: 'rect',
                                        rect: ['0px', '27px', '416px', '318px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,1.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"]
                                    },
                                    {
                                        id: 'close',
                                        type: 'image',
                                        rect: ['405px', '-15px', '63px', '62px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px']
                                    }]
                                }]
                            },
                            {
                                id: 'container-preguntas',
                                type: 'group',
                                rect: ['359', '489', '454', '141', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo-botones',
                                    type: 'image',
                                    rect: ['0px', '0px', '454px', '141px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px']
                                },
                                {
                                    id: 'btn-3',
                                    type: 'image',
                                    rect: ['328px', '44px', '56px', '56px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px'],
                                    userClass: "btn3"
                                },
                                {
                                    id: 'btn-2',
                                    type: 'image',
                                    rect: ['207px', '44px', '56px', '56px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_16.png",'0px','0px'],
                                    userClass: "btn3"
                                },
                                {
                                    id: 'btn-1',
                                    type: 'image',
                                    rect: ['66px', '44px', '56px', '56px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px'],
                                    userClass: "btn3"
                                },
                                {
                                    id: 'label',
                                    type: 'text',
                                    rect: ['22px', '18px', 'auto', 'auto', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​Preguntas</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "nowrap"]
                                }]
                            },
                            {
                                id: 'btn-atras',
                                type: 'image',
                                rect: ['19px', '47px', '77px', '95px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_30.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage-4',
                            type: 'group',
                            rect: ['0', '0', '1034', '650', 'auto', 'auto'],
                            c: [
                            {
                                id: 'base2',
                                type: 'group',
                                rect: ['0', '0', '1034', '650', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo-4',
                                    type: 'image',
                                    rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"fondo-2.png",'0px','0px']
                                },
                                {
                                    id: 'container-4',
                                    type: 'group',
                                    rect: ['847px', '240', '187', '410', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'opacity-4',
                                        type: 'image',
                                        rect: ['0px', '0px', '187px', '410px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_12.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn_cred-4',
                                        type: 'image',
                                        rect: ['64px', '286px', '58px', '73px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_10.png",'0px','0px'],
                                        userClass: "btn_f"
                                    },
                                    {
                                        id: 'btn_obj-4',
                                        type: 'image',
                                        rect: ['19px', '15px', '148px', '144px', 'auto', 'auto'],
                                        clip: 'rect(0px 119px 117px 27px)',
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px'],
                                        userClass: "btn_f"
                                    },
                                    {
                                        id: 'btn_ins-4',
                                        type: 'image',
                                        rect: ['19px', '147px', '148px', '144px', 'auto', 'auto'],
                                        clip: 'rect(0px 119px 117px 27px)',
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px'],
                                        userClass: "btn_f"
                                    }]
                                },
                                {
                                    id: 'label-3',
                                    type: 'group',
                                    rect: ['96px', '0', '928', '129', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'sub_label_3',
                                        type: 'image',
                                        rect: ['510px', '61px', '418px', '68px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px']
                                    },
                                    {
                                        id: 'label_3',
                                        type: 'image',
                                        rect: ['0px', '0px', '928px', '61px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                                    }]
                                }]
                            },
                            {
                                id: 'container-textos',
                                type: 'group',
                                rect: ['357', '129', '468', '483', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo-textos',
                                    type: 'image',
                                    rect: ['0px', '0px', '468px', '483px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"fondo-textos.png",'0px','0px']
                                },
                                {
                                    id: 'Group',
                                    type: 'group',
                                    rect: ['32', '30', '408', '421', 'auto', 'auto'],
                                    overflow: 'auto',
                                    userClass: "scroll",
                                    c: [
                                    {
                                        id: 'textos',
                                        type: 'text',
                                        rect: ['0px', '111px', '408px', '218px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​</p>",
                                        font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    }]
                                }]
                            },
                            {
                                id: 'avatar3',
                                symbolName: 'avatar',
                                type: 'rect',
                                rect: ['-6px', '275px', '424', '384', 'auto', 'auto']
                            },
                            {
                                id: 'btn-atras2',
                                type: 'image',
                                rect: ['19px', '47px', '77px', '95px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_30.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage-5',
                            type: 'group',
                            rect: ['0', '0', '1034', '650', 'auto', 'auto'],
                            c: [
                            {
                                id: 'base3',
                                type: 'group',
                                rect: ['0', '0', '1034', '650', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo-5',
                                    type: 'image',
                                    rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"fondo-2.png",'0px','0px']
                                },
                                {
                                    id: 'label-4',
                                    type: 'group',
                                    rect: ['96px', '0', '928', '129', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'sub_label_4',
                                        type: 'image',
                                        rect: ['510px', '61px', '418px', '68px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px']
                                    },
                                    {
                                        id: 'label_4',
                                        type: 'image',
                                        rect: ['0px', '0px', '928px', '61px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                                    }]
                                }]
                            },
                            {
                                id: 'avatar4',
                                symbolName: 'avatar',
                                type: 'rect',
                                rect: ['-6px', '275px', '424', '384', 'auto', 'auto']
                            },
                            {
                                id: 'container-retro',
                                type: 'group',
                                rect: ['357', '129', '468', '483', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo-textos2',
                                    type: 'image',
                                    rect: ['0px', '0px', '468px', '483px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"fondo-textos.png",'0px','0px']
                                },
                                {
                                    id: 'fail',
                                    type: 'image',
                                    rect: ['275px', '77px', '97px', '85px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px']
                                },
                                {
                                    id: 'good',
                                    type: 'image',
                                    rect: ['94px', '77px', '97px', '85px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px']
                                },
                                {
                                    id: 'retro',
                                    type: 'text',
                                    rect: ['32px', '191px', '408px', '163px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 2500,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "avatar": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'group',
                            id: 'avatar',
                            rect: ['0px', '0px', '424', '384', 'auto', 'auto'],
                            c: [
                            {
                                type: 'image',
                                id: 'fondo-redondo-Per',
                                rect: ['0px', '0px', '378px', '384px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/fondo-redondo-Per.png', '0px', '0px']
                            },
                            {
                                type: 'image',
                                id: 'Personaje-Principal',
                                rect: ['14px', '25px', '280px', '337px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/Personaje-Principal.png', '0px', '0px']
                            },
                            {
                                type: 'image',
                                id: 'brazo',
                                rect: ['209px', '251px', '215px', '101px', 'auto', 'auto'],
                                transform: [[], ['-44'], [0, 0, 0], [1, 1, 1]],
                                fill: ['rgba(0,0,0,0)', 'images/Recurso_29.png', '0px', '0px']
                            }]
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '424px', '384px']
                        }
                    }
                },
                timeline: {
                    duration: 2500,
                    autoPlay: true,
                    data: [
                        [
                            "eid35",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid255",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid256",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid257",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid258",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid259",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid42",
                            "-webkit-transform-origin",
                            1000,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid260",
                            "-moz-transform-origin",
                            1000,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid261",
                            "-ms-transform-origin",
                            1000,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid262",
                            "msTransformOrigin",
                            1000,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid263",
                            "-o-transform-origin",
                            1000,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid264",
                            "transform-origin",
                            1000,
                            0,
                            "linear",
                            "${brazo}",
                            [8,23],
                            [8,23],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid39",
                            "rotateZ",
                            0,
                            1000,
                            "linear",
                            "${brazo}",
                            '0deg',
                            '-44deg'
                        ],
                        [
                            "eid44",
                            "rotateZ",
                            1000,
                            1000,
                            "linear",
                            "${brazo}",
                            '-44deg',
                            '0deg'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
